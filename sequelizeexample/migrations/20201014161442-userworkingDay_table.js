'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable("userworkingday",{
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    workId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      workingDayId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE
    })
  },

  down: async (queryInterface, Sequelize) => {
   
  }
};
