'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable("workingdays",{
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
     weekDay: {
      type: Sequelize.STRING
    },
    workingDate: {
      type: Sequelize.DATE
    },
    isWorking: {
      type: Sequelize.BOOLEAN
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE
    
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("workingdays");
  }
};
