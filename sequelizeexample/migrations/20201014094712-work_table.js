'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable("works", {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    workMode: {
        type: Sequelize.STRING(35),
        allowNull: false,
        unique: true
    },
    userId: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE
    });
  },

  down: async (queryInterface, Sequelize) => {
   return queryInterface.dropTable("works")
  }
};
