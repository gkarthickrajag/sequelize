const multer = require('multer');
 
// var storage = multer.memoryStorage()
// var upload = multer({storage: storage});
var storage = multer.diskStorage({
    destination: (req, file, cb) =>{
        cb(null, "./upload");
    }
});

var uploadFile = multer({storage:storage});
 
module.exports = uploadFile;