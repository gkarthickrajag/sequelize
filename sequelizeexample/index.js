const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");

const userController = require('./controller/userController')
const imageController = require('./controller/imageController')
const workController = require('./controller/workController')
const workingdayController = require('./controller/workingdayController')
const userworkingdayController = require('./controller/userworkingdayController')

var app = express();
app.use(bodyParser.json());


app.use("/user",userController);
app.use("/",imageController);
app.use("/work",workController);
app.use("/workingday",workingdayController);
app.use("/userworkingday",userworkingdayController)

app.listen(3000,err => {
    if (err) return console.log(`Cannot Listen on PORT: ${3000}`);
    console.log(`Server is Listening on: http://localhost:${3000}/`);
  });






