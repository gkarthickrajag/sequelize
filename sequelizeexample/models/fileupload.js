const Sequelize = require("sequelize");
const connecction = require ("../database/connection");

module.exports = connecction.define('files', {
    id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
	  type: {
		type: Sequelize.STRING
	  },
	  name: {
		type: Sequelize.STRING
	  },
	  data: {
		type: Sequelize.BLOB('long')
	  }
	});
	
 