const Sequelize = require("sequelize");
const connection = require("../database/connection");
const User = require("./user");
const WorkingDay = require("./workingday");

    const Work = connection.define("Work", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        workMode: {
            type: Sequelize.STRING(35),
            allowNull: false,
            unique: true
        },
        userId: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
        },

    });

    // Work.belongsTo(User, {foreignKey: 'userId'})
    Work.belongsToMany(WorkingDay, {through: "userworkingday",foreignKey: 'workId', as: 'usersdays'})

    module.exports = Work;