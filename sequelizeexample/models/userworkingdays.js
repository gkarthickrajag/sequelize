const Sequelize = require("sequelize");
const connection = require("../database/connection");
const Work = require("../models/work");
const Workingday = require("../models/workingday");

var userWorkingday = connection.define("userworkingDay", {
  id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    autoIncrement: true,
    primaryKey: true
  },
  workId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    // references: {        
    //   model: 'User',
    //   key: 'id'
    // }
  },
  workingDayId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    // references: {        
    //   model: 'WorkingDay',
    //   key: 'id'
    // }
  },
});

userWorkingday.belongsTo(Work, { foreignKey: 'workId' })
userWorkingday.belongsTo(Workingday, { foreignKey: 'workingDayId' })

module.exports = userWorkingday;
