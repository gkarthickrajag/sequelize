const Sequelize = require("sequelize");
const connection = require("../database/connection");
const Work = require("./work");

  const User = connection.define("User", {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    username: {
      type: Sequelize.STRING(35),
      allowNull: false,
      unique: true
    },
    password: {
      type: Sequelize.STRING(20),
      allowNull: false
    }
  });

  //One-To-Many
  User.hasMany(Work, { as: "works",foreignKey:'userId' });


module.exports = User;