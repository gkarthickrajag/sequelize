const Sequelize = require("sequelize");
const connection = require("../database/connection");
const Work = require("../models/work");

const WorkingDay = connection.define("WorkingDay", {
    id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    weekDay: {
        type: Sequelize.STRING
      },
      workingDate: {
        type: Sequelize.DATE
      },
      isWorking: {
        type: Sequelize.BOOLEAN
      },
});

// WorkingDay.belongsToMany(Work, {through: "userworkingday", as: 'employes'})

module.exports = WorkingDay;
