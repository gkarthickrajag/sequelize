const express = require("express");
const router = express.Router();
const User = require("../models/user");
const Work = require("../models/work");
const Workingday = require("../models/workingday");
const userWorkingday = require("../models/userworkingdays");

//One-To-One
// userWorkingday.belongsTo(Work, {foreignKey: 'workId'})
// userWorkingday.belongsTo(Workingday, {foreignKey: 'workingDayId'})

//One-To-One
router.get("/:id", (req, res) => {
    Work.findOne({where:{id:req.params.id}, include:'user'}) 
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving users."
        });
      });
  });

  //One-To-Many
  router.get("/:id", (req, res) => {
      id = req.params.id
    User.findByPk(id,{include:["works"]}) 
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving users."
        });
      });
  });

  router.get("/", (req, res) => {
    Workingday.findAll({include:['employes']}) 
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving working day."
        });
      });
  });

  module.exports = router;

