const express = require("express");
const router = express.Router();
const WorkingDay = require("../models/workingday");
const User = require("../models/user");
const Work = require("../models/work");

//Many-To-Many
 WorkingDay.belongsToMany(Work, {through: "userworkingday",foreignKey: 'workingDayId', as: 'employes'})

router.post('/',(req,res) => {
    const workingday = {
        weekDay: req.body.weekDay,
        workingDate: req.body.workingDate,
        isWorking: req.body.isWorking,
    };

    WorkingDay.create(workingday).then(data => {
        res.send(data)
    })
    .catch(err=>{
        res.status(500).send({
            message:
            err.message || "some error occured while creating the working day."
        });
    });
});

router.get('/:id', (req, res) => {

    id = req.params.id,
  
    WorkingDay.findByPk(id)
        .then(data => {
          res.send(data);
        })
        .catch(err => {
          res.status(500).send({
            message: "Error retrieving working day with id=" + id
          });
        });
  });
  
  router.get("/", (req, res) => {
    WorkingDay.findAll({}) 
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving working day."
        });
      });
  });
  
  router.put("/:id", (req, res) => {
    const id = req.params.id;
  
    WorkingDay.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "working day was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update working day with id=${id}. Maybe Working Day was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating working day with id=" + id
        });
      });
  });
  
  router.delete('/:id', (req, res) => {
    id = req.params.id
    console.log(id)
    WorkingDay.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "working day was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete working day with id=${id}. Maybe working day was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete working day with id=" + id
        });
      });
  });

module.exports = router;