const express = require("express");
const router = express.Router();
const Work = require("../models/work");
const User = require("../models/user");
const WorkingDay = require("../models/workingday");


//Many-To-Many
// Work.belongsToMany(WorkingDay, {through: "userworkingday",foreignKey: 'workId', as: 'usersdays'})


router.post('/',(req,res) => {
    const work = {
        workMode: req.body.workMode,
        userId: req.body.userId,
    };

    Work.create(work).then(data => {
        res.send(data)
    })
    .catch(err=>{
        res.status(500).send({
            message:
            err.message || "some error occured while creating the work."
        });
    });
});

router.get('/:id', (req, res) => {

    id = req.params.id,
  
      Work.findByPk(id)
        .then(data => {
          res.send(data);
        })
        .catch(err => {
          res.status(500).send({
            message: "Error retrieving work with id=" + id
          });
        });
  });

  router.get("/", (req, res) => {
    Work.findAll({}) 
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving works."
        });
      });
  });

  router.put("/:id", (req, res) => {
    const id = req.params.id;
  
    Work.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "work was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update user with id=${id}. Maybe work was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating work with id=" + id
        });
      });
  });

  router.delete('/:id', (req, res) => {
    id = req.params.id
    console.log(id)
    Work.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "work was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete work with id=${id}. Maybe work was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete work with id=" + id
        });
      });
  });

module.exports = router;