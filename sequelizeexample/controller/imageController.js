const express = require("express");
const router = express.Router();
const image = require("../models/fileupload");
const stream = require('stream');
const upload = require('../config/multerconfig')

//upload
router.post('/upload', upload.single("uploadfile"), (req, res) => {
  image.create({
    type: req.file.mimetype,
    name: req.file.originalname,
    data: req.file.buffer
  }).then(() => {
    res.send('File uploaded successfully! -> filename = ' + req.file.originalname);
  })
})

//download
router.get('/upload/:id', (req, res) => {

  image.findByPk(req.params.id).then(file => {
    var fileContents = Buffer.from(file.data, "base64");
    var readStream = new stream.PassThrough();
    readStream.end(fileContents);

    res.set('Content-disposition', 'attachment; filename=' + file.name);
    res.set('Content-Type', file.type);

    readStream.pipe(res);
  });
})

module.exports = router;
