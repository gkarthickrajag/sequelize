const express = require("express");
const router = express.Router();
const User = require("../models/user");
const Work = require("../models/work");
const Workingday = require("../models/workingday");

//One-To-Many
// User.hasMany(Work, { as: "works"});
// Work.belongsTo(User, { as: "user", foreignKey: "userId" });
//many-To-Many
// User.belongsToMany(Workingday, {through: "UsersWorkingDays",foreignKey: 'userId', as: 'days'})


router.post('/', (req, res) => {
  const user = {
    username: req.body.username,
    password: req.body.password,
  };
  User.create(user).then(data => {
    res.send(data);
  })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the user."
      });
    });
});

router.get('/:id', (req, res) => {

  id = req.params.id,

    User.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving user with id=" + id
        });
      });
});

router.get("/", (req, res) => {
  User.findAll({include:["works"]}) 
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
});

router.put("/:id", (req, res) => {
  const id = req.params.id;

  User.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "user was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update user with id=${id}. Maybe user was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating user with id=" + id
      });
    });
});

router.delete('/:id', (req, res) => {
  id = req.params.id
  console.log(id)
  User.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "user was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete user with id=${id}. Maybe user was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete user with id=" + id
      });
    });
});

module.exports = router;